# Definitions #

This page specifies the meaning of various nouns and verbs used in the documentation.

**Account**: Synonym for User.  (See also Private Account.)

**Comment**: some user-created text linked to a Post.


**DashBar**: a set of buttons on the bottom of the Home Screen, submissions home screen, activity involving you screen, and search screens.

**Direct Post**: a post published to a specific set of users

**Follow Request**: an event of UserA wishing to follow UserB

**Followed Users**: a list of users that a UserA follows

**Home Screen**: a screen displaying posts made by UserA's Followed Users

**Private Account**: a setting that limits visible information and requires approval of Follow Requests.

**Post**:   (See also Direct Post.)

**Profile Screen**: a screen displaying information about a user

**Search Screen**: a screen allowing a user to view trending images and search for users

**Submissions Home Screen**: a screen displaying submissions to Pic of the Week made by UserA's Followed Users

**User View**: a screen displaying a profile screen not belonging to UserA