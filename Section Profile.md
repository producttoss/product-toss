# Profile Screen #

The Profile Screen displays information about a user to the user and to people permitted to view the user.  User can edit information with [Profile Edit](Section Profile Edit).

## Mockups ##

Self-Profile Screen

![* ...](https://bitbucket.org/repo/e96xBk/images/1029575021-user%20profile.jpg)
UserA has submitted a pic to the current weeks contest (signified by race flags) UserA now may submit another post that will add to queue (represented by POW-only UserA can see POW icon) for next weeks competition (raceflags will appear in thumbnail upon start of competition and will disappear at end of competition) if user removes pic from profile it will be removed from competition at which point another pic may be submitted (user will be able to reverse submission action without deleting post as well. see **Post View**) (special confirmation window for deleting submitted post in **Post View**, special notification window for if a user attempts to submit multiple posts to the same competition).
![profile add.jpg](https://bitbucket.org/repo/e96xBk/images/152345337-profile%20add.jpg)
User View Screen

![other user 1.jpg](https://bitbucket.org/repo/e96xBk/images/4008801263-other%20user%201.jpg)
race flags in thumbnail means user submitted that post to the current weeks Pic of the Week competition
![other user 2.jpg](https://bitbucket.org/repo/e96xBk/images/878580228-other%20user%202.jpg)

## Entrances ##
* clicking a username or profile photo
* clicking profile icon in banner on home screen, submissions home screen, search screens, and activity involving you screens.

## Exits ##
* home button in screen banner bringing user to home screen
* back arrow in banner bringing user back to previous screen
* thumbnail click bringing user to post view screen
* edit your profile button bringing user to my account

## Actions ##
* Self-Profile Screen
    * access photo library by scrolling thumbnails
    * toggle on/off submit post to Pic of the Week competition
    * write/ edit personal description
    * toggle on user profile name, then hold down until keyboard appears to edit ( will change appearance to UserB who has not designated your user profile name with a name of their choosing already) 
* User View Screen
    * follow, request to follow if user is private: toggle unfollow (if user is private refollow will require confirmation again
    * browse through users photo gallery thumbnails, click to access post view
    * block user, report user
    * toggle between username and users input name in banner-click and hold to edit/customize users input name
    * toggle no user profile name, then hold down until keyboard appears to edit ( UserB user profile will change and stay to what UserA designates)
## Notes ##

### Self-Profile Page ###
* Back arrow in banner
* Home access button in banner
* Username/ user input name toggle by clicking said text in banner

### Page will show: ###
* Edit profile/settings access
* Profile picture
* number of: followers/following/posts
* Write something button(actual text as button)/users text as edit button if already text written in. (users text may be up to 10 lines and space will expand/contract with text lines written.
* All users posts in thumbnail form
* Race flags toggle (toggle on--then click thumbnail image user wishes to submit--a window pops up asking for confirmation if user wishes to submit to Pic of the Week competition *cancel in conformation window does not toggle off race flags they must be toggled off at source.)
* Badges in lower right corner notifying submissions/prizes in the Pic of the Week competition.

### User View Page ###
* Same as self profile page, except
    * Instead of race flags toggle a settings button will be accessible with options to block or report user
    * Instead of edit profile/ settings access will be a button determining friendship status (following, requested to follow, request to follow)
    * Users written text will not be editable.