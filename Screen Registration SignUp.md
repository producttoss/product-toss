# Sign Up Screen #
* Email verification (email sent post email signup--pre username selection however confirmation not required)
* DOB min. age 13
*  Username selection (permanent+unique for identity purposes)
*  Username already exists message for duplicating username attempt (orange fail box with X to clear field and attempt a new one--field does not require to be cleared, failed entry may be edited and reattempted)
*  Users Name input (non-unique+optional)
*  Phone number verification (optional)
*  Phone verification via sms/voice call



* User logged in after sign up
* User is directed to the pre-VS. World screen

* Fields
    * Email (unique)
    * Password
    * DOB Selector
* Validation
    * Email
        * "Invalid email.": EMAILINVALID - fails email parse check
        * "Email in use.": EMAILEXISTS - email already exists
    * Password
        * "Too short.": PASSWORDTOOSHORT - len <5 characters
    * DOB
        * "Sorry! Must be 13.": DOBTOOYOUNG - age <13 years

![sign up for wiki.jpg](https://bitbucket.org/repo/e96xBk/images/1744145905-sign%20up%20for%20wiki.jpg)

# API #
* validateEmail
    * Input: email
    * Return: error state
* registerUser
    * Input: email, encrypted password, DOB
    * Return: error state, login token
    * SideEffects: send registration email

# DB #
* registration table
    * lookup by email
    * insert record with email, pass, DOB
* session table
    * insert record with login token