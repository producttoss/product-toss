![Product Toss Logo.jpg](https://bitbucket.org/repo/e96xBk/images/270609031-Product%20Toss%20Logo.jpg)

You have reached the in progress development of a mobile application called Product Toss. Welcome! Product Toss allows users opportunities through games and contests to win prizes. A PicWheel to spin and find prizes or users to follow. A design competition for users to compete in. Also there is a place for users to connect with friends. Upon logging in for the first time you can immediately be consumed with amazing submissions from the past weeks competition. No ordinary submissions. These submissions are intricate and creative masterpieces from all around the world all tied together by a shared theme (deep sea could be the theme one week, or Pluto, Maybe its coolest chia pet idea sponsored. We could collect the best new product designs for chia pets new line. Put the artists in contact with chia, put artists in line with our sponsors after all they are some of the best in the world. Just some ideas, always generating new ones.




## General Information ##
* [Definitions](Definitions)
* [Screen List](Screens)

## Sections and Screens ##
* [registration](Section Registration)
    * [Sign Up](Screen Registration SignUp)
    * [Log In](Screen Registration LogIn)
    * [Reset Password](Screen Registration ResetPassword)
* [profile](Section Profile)
* [profile edit](Section Profile Edit)
* [home](Section Home)
* [activity involving you](Section Activity Involving You)
* [search](Section Search)
* [photo upload](Section Photo Upload)
* [post view](Section Post View)
* [game](Section Game)
* [app management](App Management)
* [splash pages/ push notifications](Splash Pages)

## Draft Database Schema ##
* registration: reg data, verification, lost password
* user profile: login data, profile data, user images
* user social: followers, follow requests, blocked users
* media: photos, videos, comments
* user activity: aggregate table?
* game: photos (submitted photos only), votes: **see voting structure (below)**

## Game Mechanics and Structure: ##

Each weekly competition has 3 stages. There are 3 weekly contests running/ overlapping at one time.

**Stage 1**: Theme announcement and submission stage- users have 1 week after a theme is announced to design and submit their artwork.

**Stage 2**: all submissions are pooled and user voting starts. Finalists are filtered out by approval ratings. The last day of this stage is reserved for final voting and winners are chosen.

**Stage 3**: winning submissions are put on display in the winners gallery where they may be viewed, shared and recognized by all users.

At close of stage 1 a new theme is announced and a new competition associated with that theme is launched into entry submission stage (stage 1). This is what I explained as overlapping. 

At any one time there are 3 competitions running. One competition in stage 1, one competition in stage 2, and a third competition in stage 3.

At end of stage 3 the themed competition is archived.

Please review this diagram (below) that spans 3 weeks. Rectangles represent contest spans, colors represent themes, numbers represents stages.
![contest diagram.PNG](https://bitbucket.org/repo/e96xBk/images/2845135628-contest%20diagram.PNG)

???Maybe the cycle change is every Sunday? Or Monday night not sure what would be the best day. Maybe Friday. Also what time it should be at. I think midnight.???

**(Addendum (7/18/2015))**

Addendum titled: **Themes/ Contest Structure**

  *  one contest a week may not have the engagement desire for the consumer. it could be possible to set up the themes to 1-2 theme contests per day. that way there would be an undetermined but increased number of contests to submit work to and an undetermined but increased number of galleries vote on at any time.
       *   For submitters= users who submit artwork to contests will be able to review different themes and date/time when submission period for the given theme ends. **Things to think about:** maybe the submitters can save and organize the themes they want to submit artwork/ images/ videos to into a favorites section where they can easily revisit/ review the upcoming submission end dates. also how is the voting going to work? is it going to be lock and load. is it going to be lock and load? or are we going to lock in submissions and then start the voting gallery for the contest. every themed competition is going to have the same structure? it may get confusing to users if not. what are the timelines? the number of days for each stage in the competitions cycle? the competition cycles might get shorter as we build a user base. 

**(End addendum)**

##Voting structure##
Since people are going to be submitting throughout the week prior to the voting we can review the submissions and delete the crap (users won't know if their submission is floating around in the voting galleries or not) Also we can note the great submissions and give them immunity through the first few voting rounds so they don't get voted out by mistake.

*  submissions are given a specific serial number for identity. these numbers remain unique.
   *  two submissions are matched face to face at a time. user votes for either/ or. 
      *  submissions are ranked based on these results; sorted and filtered.



**Possible product addition possibility**


![hashtag contest.jpg](https://bitbucket.org/repo/e96xBk/images/210980874-hashtag%20contest.jpg)
![hashtag contest archives.png](https://bitbucket.org/repo/e96xBk/images/2655510603-hashtag%20contest%20archives.png)
-end

# The following is introductory info from Bitbucket #

Welcome to your wiki! This is the default page we've installed for your convenience. Go ahead and edit it.

## Wiki features

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax.

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

Go ahead and try:

```
$ git clone https://pickanon@bitbucket.org/pickanon/picanonymous.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.

## Syntax highlighting


You can also highlight snippets of text (we use the excellent [Pygments][] library).

[Pygments]: http://pygments.org/


Here's an example of some Python code:

```
#!python

def wiki_rocks(text):
    formatter = lambda t: "funky"+t
    return formatter(text)
```


You can check out the source of this page to see how that's done, and make sure to bookmark [the vast library of Pygment lexers][lexers], we accept the 'short name' or the 'mimetype' of anything in there.
[lexers]: http://pygments.org/docs/lexers/


Have fun!