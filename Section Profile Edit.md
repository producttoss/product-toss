# Edit Profile Screen #

**Profile Edit** allows the user to change information that appears on user's [profile page](Section Profile) as well as their shipping information for if they win a prize.
**My Account** allows the user to change settings for the user's account.

## Mockups ##

![edit profile 1.jpg](https://bitbucket.org/repo/e96xBk/images/683752163-edit%20profile%201.jpg)


![verify phone in edit profile.jpg](https://bitbucket.org/repo/e96xBk/images/3870045694-verify%20phone%20in%20edit%20profile.jpg)

verify phone from edit profile screen ^
same as phone verification in registration minus the skip button

![save email in settings.jpg](https://bitbucket.org/repo/e96xBk/images/4265837846-save%20email%20in%20settings.jpg)
verify e-mail save results in email confirmation same as in registration ^

My account button access:
![my account .jpg](https://bitbucket.org/repo/e96xBk/images/1814451926-my%20account%20.jpg)
![change password.jpg](https://bitbucket.org/repo/e96xBk/images/1563449917-change%20password.jpg)
if white check mark is clicked message appears ^

![change password 2.jpg](https://bitbucket.org/repo/e96xBk/images/2248141215-change%20password%202.jpg)
after clicking submit button user is directed back to My Account Screen ^

![blocked users.jpg](https://bitbucket.org/repo/e96xBk/images/3339490543-blocked%20users.jpg)

## Entrances ##
* access from self-profile screen

## Exits ##
* cancel button to self profile screen, save confirmation/cancel without saving if information is edited
* access phone number verification screen (settings)
* access email information settings (email sent upon confirmation of changed email)
* access my account
* logout 

## Actions ##
* edit information directly in input fields on screen
* information requires initial entry, edit information if fields already populated

## Notes ##
### Edit profile ###
* (save changes required)
* (confirmation window if exit attempted without saving)
* Cancel/save in banner
* Personal information input
* Phone number entry
    * (new screen for change/new)
    * (verification required)
* Email entry
    * (new screen for change/confirm)
    * (verification recommended)
* Change profile image
    * (upload from camera roll)
    * (crop profile image page appears fit then change or cancel to exit out without changing)
* Privacy toggle on/off
* Log out (confirmation window appears)
* My account

### Account settings access ###
* Back or home button in banner (confirmation window if exit attempted- To home without saving.. (only if edits made))
* Change password settings
* Preload Video Settings (For Gameplay)
* Blocked users settings (access for unblocking)
* Terms of use screen access
* Privacy Policy screen access