**push notifications**

* end of competition at weeks end notification: 

"Its the weekend, that means votes are in and a new winners gallery is available for view! Login to start voting on the new theme. It is time to start working on your next weeks submissions well."

* push notification for finalists:

"Congratulations! Your submission has made it to the final round of voting! stay tuned."

* push notification for winners:

"Congratulations!! you won the week! login to see your submission in the winners gallery, we added a badge to your profile. We will contact you shortly with information about your prize."

* push notification to random users:

"Congratulations! You have been chosen to to win a guaranteed prize on your next spin of the Pic Wheel!"