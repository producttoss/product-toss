# Reset Password #
* Support for password reset through email
* Pin generation sent to e-mail (no matter confirmed or not confirmed)
* Password reset options require pin (copy and paste or manual input)
* Password failure notification requirements/messages based on user input (ex. Minimum 8 characters)
* incorrect code: PROCEED button does not appear
* bad password: error message, green check does not appear

![forgot password 1.jpg](https://bitbucket.org/repo/e96xBk/images/2706542959-forgot%20password%201.jpg)
![forgot password 2.jpg](https://bitbucket.org/repo/e96xBk/images/3390707652-forgot%20password%202.jpg)
![forgot password 3.jpg](https://bitbucket.org/repo/e96xBk/images/379470515-forgot%20password%203.jpg)

# API #
* validateEmail
    * Input: email
    * Return: error state
* requestPinCode
    * Input: email
    * Return: error state, pincode
    * SideEffects: populates pincode table
* validatePinCode
    * Input: pincode
    * Return: error state
* resetPassword
    * Input: pincode + encrypted password
    * Return: error state
    * SideEffects: deletes pincode record, updates registration password

# DB #

* registration
    * lookup by email
    * update password
* pincode table
    * lookup by pincode
    * insert record with email, pincode
    * delete by email,pincode