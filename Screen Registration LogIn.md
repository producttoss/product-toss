# Log In Screen #
* Username or email for login/ password (incorrect password message/ resets password entry field only)
* Login password is masked
* Forgot password button => Support
* User is directed to the home screen.
* Fields
    * Username / email
    * Password
* Links
    * Log In
        * Validates username/password,
        * Success: signed in, loads Home screen
        * Failure: display error
    * Forgot your Password?
        * Loads Support screen
* Validation
    * Username / email
        * "Invalid username" (min char len?, character set?)
        * "No such user" (username doesn't exist)
        * "No such email" (email doesn't exist)
    * Password
        * "Sorry! Incorrect password." (password incorrect)
    * Generic
        * "Sorry! Try again in a few minutes." (timeout)

![login for wiki.jpg](https://bitbucket.org/repo/e96xBk/images/1836179413-login%20for%20wiki.jpg)

# API #

* authenticateUser
    * Input: username + encrypted password
    * Return: error state, login token

# DB #

* registration table
    * lookup by username
    * lookup by email
* session table
    * insert record with login token
    * lookup by login token
    * ? how to deal with multiple device login?
    * ? session expiration?