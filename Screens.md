The following are the distinct screens in the app.

1. Log In/Sign Up
1. Sign Up
1. Verify Email
     * email verification sent post-signup click. (verification through link in email: link to web page screen)
1. Verify Phone
     * phone verification code sent via text message or phone call.
     * phone verification code entry to proceed.
1. Log In
1. Lost Password/Support
    * email entry for recovery code request.
    * email sent to user email.
    * verification code entry to proceed.
    * change password.
1. Profile
1. Edit Profile
    * Verify Phone
       * * phone verification code sent via text message or phone call.
       * * phone verification code entry to proceed.
    * email settings screen
       * * email verification sent post-change. (verification through link in email: link to web page screen)
    * edit profile image: camera roll inventory choice screen
       * * scale and crop profile image screen
1. My Account
    * change password
    * preload video settings
    * Blocked Users
1. Privacy policy
1. Terms of Use
1. Dashboard
1. View Post
1. View post nomination list (of users who nominated the post)
1. Search
    * pre-search screen with popular submission thumbnails
    * search users
    * search user result lists
    * search hashtags
    * search hashtags result lists
    * hashtag result choice clicked lists
1. user view (other user)
1. Camera
    * pre-capture/upload screen.
    * choose from phone gallery screen.
    * crop, add caption, and share options.
1. User Home
    * From DashBar: Home is the screen with live feed of uploaded post by users user-A follows.
    * Pic of the Week submissions home toggle on/off from home using race flags in banner.
1. Vs. World Zone
    * prizes information
    * PicWheel
    * Winning submission results
       * * Calendar for access to winning submission results archives
1. Activity Involving You