# session #
* Signed in system users.
* Fields
    * user_id (index)
    * login_token (unique, primary index)
    * lastactivity_date (if idle-out is needed)
    * ip_addr
    * insert_date

# registration #
* System users.
* Fields
    * user_id (unique, primary index)
    * email (unique, index)
    * DOB
    * lastlogin_date
    * passwordchange_date
    * disabled
    * ip_addr
    * insert_date

# resetpassword #
* Temporary access to reset password.
* Fields
    * pincode (unique, primary index)
    * email
    * ip_addr
    * insert_date

# profile #
* User identity and information.
* Fields
    * user_id (unique, primary index)
    * email (from registation)
    * DOB (from registration)
    * thumb_url
    * profile_text
    * first
    * last
    * phone
    * street
    * city
    * state
    * zipcode
    * country
    * private_profile (bool)
    * searchable_profile (bool)
    * users_blocked(user_id_A, user_id_B, ...)

# friends #
* Relationship between users.
* Fields
    * source_user_id
    * target_user_id
    * Type (BLOCK, FOLLOW, REQUEST)
    * update_date

# posts #
* Fields
    * post_id
    * user_id
    * thumb_url
    * url
    * insert_date

# comments #
* Fields
    * comment_id
    * post_id
    * user_id
    * text
    * insert_date
    * List<user_id> mentionedUsers

# userEvents #
* ? or generate on the fly?
* hard to hide notifications if otf.


