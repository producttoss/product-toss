**user management**

**notes**

* tabs all have search bar
      * query only searches current tab.
* only certain tabs have scroll bar 
* blue text are links
* folders are links

organized in one single page with screen tabs at top consisting of:

* directory
      * user directory-links to individual profile information
         * link includes all user input information (status: verified or un-verified) as well as username link to profile in app. also included is users prize history and links to users posts
         * admin may ban user or freeze users account from user profile information page 
      * directory is alphabetized by username (folder per letter) and has quick search toolbar (searches all folders)
         * individual folders has quick search toolbar as well
 ![directory tab.jpg](https://bitbucket.org/repo/e96xBk/images/3966176672-directory%20tab.jpg)
![Dirfectory folder search.jpg](https://bitbucket.org/repo/e96xBk/images/3243753698-Dirfectory%20folder%20search.jpg)
Search query only this folder

![user profile for administration.jpg](https://bitbucket.org/repo/e96xBk/images/837554622-user%20profile%20for%20administration.jpg)
![user profile for administration 2.jpg](https://bitbucket.org/repo/e96xBk/images/589536993-user%20profile%20for%20administration%202.jpg)

* reports
      * reported images are organized by post links with highest number of reported posts at the top
      * upon clicking link admin is shown image (with username) and comments (with usernames)
      * post may be removed from app (resulting in all comments removed as well)
      * individual comments may be removed
      * resolved button appears at top of report once clicked the report is moved to resolved report section (sub-folder in reports)
      * usernames link to directory page where user profile information is stored (back arrow returns admin to report)
![reports tab 1.jpg](https://bitbucket.org/repo/e96xBk/images/242130639-reports%20tab%201.jpg)
reported posts show up here
![reports tab 2.jpg](https://bitbucket.org/repo/e96xBk/images/2270193033-reports%20tab%202.jpg)
posts that were removed were moved here, post is removed from app (post deletes after 1 month)
![reports tab 3.jpg](https://bitbucket.org/repo/e96xBk/images/2722904327-reports%20tab%203.jpg)
posts that were resolved were moved here, nothing happens to post in app (post deletes after 1 month)
![report.jpg](https://bitbucket.org/repo/e96xBk/images/3178966250-report.jpg)

* banned/ frozen accounts
      * toggle between banned user list (permanent) and frozen user list (temporary)
      * banned user-account inaccessible by user (account inactive message)
      * users may unfreeze a frozen account without admin help by e-mail 
         * at login attempt user is denied access with account frozen message. user is sent an e-mail with a link to reset password
![frozen.jpg](https://bitbucket.org/repo/e96xBk/images/2058871796-frozen.jpg)
![banned.jpg](https://bitbucket.org/repo/e96xBk/images/2840198405-banned.jpg)

* emails
      * email presets will be saved here for all administrative e-mails
      * email list
         * confirm email
         * password changed
         * consecutive wrong passwords frozen account
            * asking username to send an e-mail to unfreeze account to the address: frozen_account@producttoss.com with note saying: only emails with username in the subject line will be read.
         * prize winner emails:
            * user email or phone not verified: requiring user to verify before reception of gift
            * user account shipping information not complete: 
                  * required user to finish filling information and reply back with username and confirmation# in email
                  * individual confirmation number placed on each individual prize not sent issue 
                  * email response to prize_deployment@producttoss.com
      * emails sent from no_reply@producttoss.com
      * (i am not sure how this formats so further information later)
* push notifications
      * push notifications can be sent from here (i am not sure how this formats so further information later)
      * see push notifications section for information on push notification presets.
* contest center
      * theme entry bar will appropriate text across the entire app in designated locations
      * upcoming contest section
         * submissions will populate this section
      * current contest section
         * submissions will be shown with number of votes ( highest voted will appear at top of list)
         * submissions can be removed from the competition from here (users will not know if their submission is still in the competition or not)
         * pic wheel entry input accessed from here (sorry not a winner.. follow user?) admin can select users pics to input into the pic wheel not a winner message field
      * last competition winners section
         * set up for archive of winning entries. winners are pushed up at end of competition. this field is then made blank 
      * pic wheel prize winners
         * shows shipping information and link to profile as well as e-mail address. If users information is not complete order is put on hold and user is e-mailed
         * shipped prizes are marked completed
* archives
      * archives will be saved here formatted for compatibility with the applications archives section

**Dummy Account**

admin dummy account in app allows admin to search all profiles (including account private access) no access to direct posts from here. only direct posts that are reported need review (see reports section)