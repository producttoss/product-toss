# Home Screen #

The Home Screen displays a "news feed" of posts for UserA's Followed User List.  It also displays a DashBar of navigation to other Screens.

## Mockups ##

![home 1.jpg](https://bitbucket.org/repo/e96xBk/images/2520718733-home%201.jpg)
toggle nominate/nominated on/off

![* ...](https://bitbucket.org/repo/e96xBk/images/793621481-Home%20Screen.jpg)

## Entrances ##
* access from pages displaying a home icon in the banner: My account, VS-World, self-profile view, user view, post view
* access from exiting camera screen
* access from pre VS-World zone and VS-World zone
* access from activity involving you screens through dash bar
* access from search screens through dash bar

## Exits ##
* click post
* click username
* click dashbar icons (all other than home icon)
* click profile icon in banner
* toggle between home and submissions home by clicking race flags in banner
* click # of nominations below post
* click comment button below post

## Actions ##
* scroll through news feed up and down
* nominate posts by clicking nominate button below post (toggle on/off)
* report posts
* tweet posts (requires confirmation)
* share post on Facebook (requires confirmation)
* video playback features
    * * click to play video (play icon in center of video pre-click)
    * * click (toggle on/off) video for sound (sound always starts off)
    * * video stops after playback, click replay (sound settings in replay repeat from play before)
    * * * sound setting resets when post is scrolled off screen

## Notes ##
* Consists of Live news feed of posts made by the people user is following
* By clicking the race flags icon in banner=User may toggle between the weekly pic submissions (for the upcoming week) made by the people they are following or all recent posts made by the people they are following.
* If user is not following any people news feed will be blank. Instead of being a completely blank news feed user will view a message telling them “Your news feed is blank; find people to follow.” We will suggest options for the user to find friends from facebook, twitter, and/or their address book.
* If user is following people who have not made any submissions to the pic of the week contest for the upcoming week and they toggle to the submissions feed they will receive a message suggesting for them to follow more people and show the options for finding friends from FB, Twitter, and address book.
* News feed scrolls- newest post displays at top of the list and in descending order by timestamp. Posts may be nominated (clicking nominate button does not leave the live feed page), clicked on to view comments (leaves live feed page and drops user into image feed-thumbnail with username who made the post in banner and comments below), or commented on (clicking the comment button will result in directly entering the comment text entry bar in the image feed page)
* If user clicks the green button below the image where it says the number of nominations= user will then drop out of news feed into a scrolling list with all the names and thumbnail profile images of the users who nominated the post. Each user in this list may be followed directly from this page or may be clicked on and linked to their profile.
* Profile link: in the banner on the right side is a button that will send a user to their profile page

### DashBar ###
* On the bottom of the live feed is a DashBar consisting of 5 buttons. (these buttons will have gaps between them showing the page behind but to avoid confusion the voids will be shielded so a user will not accidently click in these voids on an active link in the page behind.

The buttons access (From left to right): User Home, Search, Vs. World Zone, Camera/Uploading, and activity involving you.


## API ##
* getNewsFeed
    * Input: sessionId
    * Output: error status, list of news events
* nominatePost
    * Input: sessionId, postId
    * Output: error status
* reportPost
    * Input: sessionId, postId
    * Output: error status

## DB ##
* newsEvents
    * Similar to user events: pre-generate, generate-at-request, load from aggregate tables with a cache?
    * Max Number?  TTL?