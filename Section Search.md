# Search Screen #

The Search Screen allows a user to view trending images and to find posts.

## Mockups ##
![search 1.jpg](https://bitbucket.org/repo/e96xBk/images/1114215960-search%201.jpg)
![search 2.jpg](https://bitbucket.org/repo/e96xBk/images/996135498-search%202.jpg)

## Entrances ##
* clicking Dash Bar "Search" icon from home screens and activity involving you screen

## Exits ##
* click on image
* click on username
* click on hashtag result
* click on any dashbar icon other than search
* click on profile icon in banner 

## Actions ##

* click search bar to input search text into field
* click trending submissions thumbnail pre-search (scroll through list)
* click to toggle between # tag search and user search
* scroll through results for either search setting
* clicking hashtag result shows thumbnail gallery of posts with said hashtag associated with the posts
* clicking user search result shows user view screen
* clearing search bar clears results (deleting letter by letter broadens search)

## Notes ##
* When clicking search button in the dash bar: user is directed to search page.
* user will have access to profile page by clicking icon in banner
* On search page the user is displayed a text entry bar with search written in the text entry bar. This bar resides directly below the page banner.
* Below the search text entry bar is a series of thumbnail images. These images are the trending images from current pic of the week competition Based on the popularity of the images (# of up votes) Images high in the trending thumbnails have a probable chance to be in the list of winning entries of that weeks competition and displayed in the VS. World zone the following week. The thumbnails scroll up and down descending from most popular to less popular. If user scrolls far down enough more thumbnails load. If these thumbnails are clicked the image feed for that image will appear.
* A users posts who has a private profile will not appear in this list even if it is most popular. (further description of this can be seen in pic of the week competition information details)
* Search text entry bar does not scroll with the thumbnails. It remains attached directly below the page banner.
* If user clicks the search bar the thumbnails disappear. In the place of the thumbnails is two toggle buttons consisting of #Tag search and User search. The user may either search through a directory of the Apps hash tags or a directory of the Apps usernames, Users name input fields, and profile description input fields. (results will be shown by relevancy in that order meaning if you search: BigTime. The results list will first show the username bigtime at the top, followed by the result with a users input name Bigtime, and show after that a users input name: Big Time (result with space between search character is less relevant) And will lastly show the results for a user whose username and user input name is irrelevant yet has the term Big Time written in their profile description.)
