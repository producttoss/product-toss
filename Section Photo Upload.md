# Photo Upload #

Photo Upload allows a user to create a Post.

## Mockups ##
![photo 1.jpg](https://bitbucket.org/repo/e96xBk/images/1058597507-photo%201.jpg)

pre capture/ pre upload ^ (user always starts out with photo toggle on upon entering)

![photo 2.jpg](https://bitbucket.org/repo/e96xBk/images/3004862631-photo%202.jpg)
post capture^ return arrow sends user back to pre capture screen.
![photo addition.jpg](https://bitbucket.org/repo/e96xBk/images/2513741143-photo%20addition.jpg)

![photo 3.jpg](https://bitbucket.org/repo/e96xBk/images/1855421262-photo%203.jpg)

![photo 4.jpg](https://bitbucket.org/repo/e96xBk/images/4245012326-photo%204.jpg)

![photo 5.jpg](https://bitbucket.org/repo/e96xBk/images/428823231-photo%205.jpg)
post capture pause/ play features.. appears at window click, fades about .3 seconds after window click. caption area void from pause/ play

## Entrances ##
* from screens with dash bar: activity involving you screens, search screens, and home screens

## Exits ##
* submitting post returns UserA to home screen
* click x icon in banner returns UserA to home screen

## Actions ##
* capture photo/ Video (toggle bar)
* rotate camera
* flash toggle (on/off/auto)
* video capture (tap to start capture tap to end capture)
* photo/ video upload
* upload crop feature in post capture screen (negate edit feature for in app captures)
   * * video cropping to length requirements if video exceeds 12 seconds (Uploaded videos are auto cropped to center of video for fit)
   * * photo scaling required if uploaded image is not formatted to a square window
* add caption to photos and video
* Facebook and Twitter share toggles
* Raceflags button toggle for submission to Pic of the Week competition (automatically creates post to UserA profile)
* toggle from sharing between all followers and directly to select followers
* revert icon returns back to pre-capture/ pre-upload screen

## Notes ##

### Pre-Capture Upload ###
* Photos and videos may be captured using the camera function. Photos and videos may also be uploaded from the users personal camera roll.
* Camera access as well as Personal camera roll access must first seek users permission.
* Camera in pre-capture/upload state will have an x button in banner allowing for user to exit out of camera and access back to home screen.
* A toggle function for capturing photo or video will be accessed above capture button(color coded). Capture button will adjust to match the color of the color coded toggle selected (between photo and video)
* Rotate camera function available
* Flash on/off/auto toggle available.

### Post capture upload ###
* Accessible in banner will be 4 options (from left to right) undo capture/ revert to pre-capture, tweet toggle on/off, Facebook share toggle on/off, and add caption bar toggle on off.
* Share toggles for FB and twitter will not require additional confirmation windows. Only required is pre-allowed App access to Twitter and Facebook in settings. If user has not already allowed access a window will appear asking if user would like to allow access to twitter if sharing with twitter or facebook if sharing with facebook. If user has not pre allowed access to either and attempts to share with both they will receive 2 consecutive windows asking for permission to share one first, then the other.
* If user attempts to undo post capture and retreat back to pre capture a confirmation window will appear asking if user is sure.
* Caption bar button will toggle a light grey bar in capture that may be dragged upwards and downwards. A user may type up to four lines of text in this bar. The bar will expand and retract in coherence with the number of text lines written.
* If Caption bar button is deselected and then reselected the text written in the caption bar prior to deselect will not clear. User must manually delete text in order to reset the caption bar.
* Posts may either be shared directly to 1 or multiple users and will not appear publicly or they can be shared to all followers and will appear on the users profile and live feed.
* A directly shared post may only be shared with confirmed people the user is following. The user does not need to be followed by a user they are following to share direct with that user. If a user is sent a direct post they will be able to find it in the activity involving you page accessible through the bash bar.
* Share button will toggle by color corresponding to either share option selected weather it be to followers option or direct option..
* Photos uploaded from camera roll will need to be cropped to fit the square shape of the image window. Cropping may be done directly in the upload window prior to posting them in the app.
* Uploaded videos will have a time limit of 15 seconds. Videos will go first through the crop screen same as the photos do-followed by a time crop function allowing for the user to crop up to a 15 second time window out of the video if the video is longer than 15 seconds. A play bar at the bottom of the window will allow for user playback. Videos will loop with this function.
* The size crop feature will be limited to the shortest length of the uploaded post preventing any blank space in the post.
* Race flag toggle may be used if the user wants to submit their pic of the week post directly from upload/capture.