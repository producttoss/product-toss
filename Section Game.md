# Gameplay ##

**Mockups**
![vs 1.jpg](https://bitbucket.org/repo/e96xBk/images/3499517011-vs%201.jpg)
![vs 2.jpg](https://bitbucket.org/repo/e96xBk/images/335793712-vs%202.jpg)
![vs 3.jpg](https://bitbucket.org/repo/e96xBk/images/3246267106-vs%203.jpg)
![vs 4.jpg](https://bitbucket.org/repo/e96xBk/images/2566725421-vs%204.jpg)
![vs 6.jpg](https://bitbucket.org/repo/e96xBk/images/947770164-vs%206.jpg)
![vs 7.jpg](https://bitbucket.org/repo/e96xBk/images/3769341906-vs%207.jpg)
![vs 8.jpg](https://bitbucket.org/repo/e96xBk/images/487147537-vs%208.jpg)
![vs 9.jpg](https://bitbucket.org/repo/e96xBk/images/1614666429-vs%209.jpg)
![vs 10.jpg](https://bitbucket.org/repo/e96xBk/images/3875123617-vs%2010.jpg)

**VS. World Zone**

* The center button on the Dash bar is the button leading to the VS World Zone
* User is directed to a page without the dash bar (this is the same page the user is directed to after registration and after login: Pre-VS. World Screen)
The **Pre-VS. World Screen** with home button in banner, posts of last weeks winning pic of the week entries (scroll up and down through posts) and orange submissions gallery entry button on the bottom. 
* Area in between the pic of the week entry and the page banner will have (on the left side) a thumbnail of the winners profile image and their username which will link to the users profile page. If the user has a private profile the link to their profile page will not be live.  (on the right side) will be a three line icon with a pop up toggle window. This window will have the theme for the past weeks submissions and share with facebook/twitter buttons.
The window will scroll down with an auto lock feature so UserA may flip through all the winning submissions from the past week (swipe up on the image window for the pic of the week and the silver medalists pic appears with all associated info, swipe up again and bronze appears, swipe up again and the first honorable mention appears. swipe down and the bronze medalists pic appears again and so on.
If the user clicks the Submissions Gallery button it brings them to **Gameplay**

**Gameplay**

* A new window appears with a central banner and a submission above and below.
* This page has an upward/downward  scroll because the square posts and central banner do not entirely fit in the screen. 
* The central banner consists of three buttons (from left to right) the vote button with up arrow (clicking results in voting for the top submission) the central button which toggles a window with the theme for the week and a second toggle button allowing for the user to report the submission innapropriate (this will report both submissions but the variety of matchups will result in the reports following around the inappropriate one) the third is a vote button with down arrow (clicking results in voting for the bottom submission)
* It is imperative that we make a fair voting system across the submission base. At first we can kind of fudge it by filtering out the lousy submissions but as the user base grows we can start to collect averages on the number of votes happening and designate a certain number of votes per image and filter them through the rounds. There will be a system we can create to animate this best practice.
* To leave this screen the user simply swipes the screen left to access the VS. World Zone and Right to go back to the Pre VS. World screen.
* For the user to progress further into the VS. World Zone they must vote on a minimum of five submissions daily. Upon entering the gameplay section of the app the user may swipe right at any time they like to exit out of the gameplay and back to the pre VS. World zone. Only once the user votes on five submissions they will then have full access to the VS. World Zone. 
* After voting five times in Gameplay the user may exit out after revisiting gameplay either by swiping left or right at no minimum number of votes. User retains full access to the VS. World Zone for that day only once they have voted five times.
Users are displayed different submission matchups upon reentering gameplay. If a user swipes out of gameplay the existing matchup will not be the same as when they reenter gameplay.
* Scroll function will determine precedence of play access in gameplay. If more of screen shows upper entry then upper video will have playback access while if the lower screen shows more then lower screen will have playback access (this is assuming both entries are videos)
* Video and picture entries will be matched up randomly, entries will have no differentiation between each other.

**VS. World Zone**

* Home button in banner
* Submissions Gallery button at bottom bringing user back to Gameplay
* Three Buttons in screen: Prize Zone, Pic Wheel, Last weeks winning entries.

**Prize zone**

* Description of prize for the upcoming week with image
* List of prizes available in the pic wheel
* Complements for last weeks pic of the week winner and description of what they won.
* Maybe a horizontal bar that rolls across the screen with live updates of name, description of prizes and location of winners. This bar can be swiped at resulting in speeding the bar(to an extent), sending the bar the opposite direction (to backtrack/ re-review a winner they just saw.) This bar would resemble the bar you would see on cnn or sports center with updates of stock or game results.
Below the scrolling bar is an orange button that says “go vote” and brings a user back to gameplay.

**Pic Wheel**

* only allowed one spin per day. 
* User share on facebook or twitter (of past winning submission in the Winning entries section(view Last Weeks Winning Entries section below for details)) will result an allotted a second spin that day. (limit one per week)
* Pic wheel squares revolve while white question mark remains stationary hovering in front of the wheel at result spot. Friction occurs when each square enters and exits the question mark spot. Wheel may be spun in either direction.
* Pic wheel result screen will either show a prize or a sorry not a winner message with an image of a popular Pic of the Week submission entry that links to the users profile. (back button on users profile will bring them back to the VS. World homepage.) After exiting the result window the wheel is faded out with a thank you message saying to come back tomorrow and a back arrow to access the VS. World Zone.

**Winning**

* users shipping info required to receive prize (we will e-mail suggesting user to fill in reception info, we will also send user a notification to their Activity Involving You section suggesting they fill out their shipping information)
* if users shipping information is all complete we will send them a notification that their prize is on its way to their activity involving you section and e-mail them shipping confirmation when package has sent. or if the prize is for in store pickup we will e-mail them the details and send them a notification suggesting they review the e-mail we sent to their Activity Involving you section.

**Last Weeks Winning Entries**

* Badged images (badges identify standings) appear in screen. The images may be clicked and will link to the winning users profile (if their profile is public) (back arrow on profile will link the user back to the winning entries page)
* This screen will scroll showing first prize entry through third prize and 20 honorable mentions after that. (Honorable mentions will all have the same badge) Staff determines the order of display for the Honorable mentions.
* Each winning entry will have facebook/twitter share buttons allowing for the user to share and will result in them winning a second spin of the pic wheel for that day (as mentioned in pic wheel section)
* Banner on left side has back arrow and on right side has a button for winning **submissions archives**. A window appears with a calendar that may be scrolled through by week and month (year after a while) if the user clicks a week the results from that corresponding week appear in the winning entries screen.
* If archives button in banner is revisited the window with the calendar will be exactly where it was before when the user submitted to view the archive they had just selected prior. If the user exits out of the submissions gallery the archive is reset to the most current winning entries. 