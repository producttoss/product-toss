Registration comprises the actions and workflows needed to create a user account and authenticate that user.

# Splash Screen #
* Displays when app opened for non-signed in user

![test for Wiki.jpg](https://bitbucket.org/repo/e96xBk/images/2644425475-test%20for%20Wiki.jpg)

# Sign Up Screen #
* Allow user to create an account.
* [Sign Up](Screen Registration SignUp)

# Log In Screen #
* Allow user to sign in to account.
* [Log In](Screen Registration LogIn)

# Log Out #
* Allow user to disconnect app from account.
* See edit profile page

# Reset Password #
* Allow user to recover account with forgotten password.
* [Reset Password](Screen Registration ResetPassword)

# Email Verification #
* mailed after sign up
* not required
* verification through e-mail web link

# Phone Number Verification #
* text message or voice call
* not required
* incorrect code: PROCEED button does not appear

![verify phone number 1.jpg](https://bitbucket.org/repo/e96xBk/images/2812132053-verify%20phone%20number%201.jpg)
![verify phone number 2.jpg](https://bitbucket.org/repo/e96xBk/images/2222566621-verify%20phone%20number%202.jpg)

# App Navigation #
* If user is already logged into the app and the app is not open= they are directed to the home screen.
* If the user currently has app open in background when they return to the app they will find themselves at the page they left off when they exited out prior.