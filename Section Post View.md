# Post View Screen #

The Post View Screen displays a Post consisting of a Photo/Video, nomination count, list of comments.

## Mockups ##
![post view 1.jpg](https://bitbucket.org/repo/e96xBk/images/386828175-post%20view%201.jpg)
if comment button is clicked the keyboard appears with the comment bar above. if user scrolls to bottom of comments comment bar appears as demonstrated above (if comment bar is clicked keyboard appears with comment bar above
![post view addition.jpg](https://bitbucket.org/repo/e96xBk/images/510257921-post%20view%20addition.jpg)
post view of UserA's post by UserA
![post view 3.jpg](https://bitbucket.org/repo/e96xBk/images/147393493-post%20view%203.jpg)
post view of UserB's post by UserA
## Entrances ##
* clicking on a Post elsewhere

## Exits ##
* click username
* click nomination button
* click back arrow in banner returns to previous screen
* click home button in banner returns to home screen
* click username or profile image thumbnail of commenter in comments section
* click # of nominations

## Actions ##
* nominate post
* comment on post
* if video- playback features, see video playback in home screen specifications
* if post belongs to UserA: post may be deleted, shared with twitter, shared with Facebook
* if post belongs to UserB: post may be reported, shared with twitter, shared with Facebook
* scroll down to review comments
* load comments as scroll if in excess of 10
* swipe left and right to travel through source users (same for UserA and UserB) gallery in chronological order by date

## Events ##
* add comment
* delete comment
* load additional comments
* view nomination list
* delete post
* share post

## Notes ##
Everything below banner scrolls except nomination button and comment button, scroll ends when comment bar is directly above nomination and comment buttons, auto scroll to bottom when comment button is clicked(nominate and comment buttons are replaced with post and cancel buttons when comment bar is accessed), nominate button toggle on/off to nominated button

Post window below banner same size for photo/video

Below post:

* Number of nominations button: If user clicks the green button below the image where it says the number of nominations: user will then drop out of news feed into a scrolling list with all the names and thumbnail profile images of the users who nominated the post. Each user in this list may be followed directly from this page or may be clicked on and linked to their profile.
* User comments: any user (including yourself) may comment here if they can access.. based on permissions weather public, private or direct.
* User may delete any comment on their post or any comment they make on any post (swipe left over comment and delete button appears)
* Comments structured with user profile image thumbnail on left, then username next to it (both username and thumbnail link to users profile) followed by comment.
* At right below post: triple dash button opens pop up window with 3 options (delete post, share with Facebook, Tweet options for users self-posts) (report post, share with Facebook, Tweet options for users self-posts) each option requires confirmation window.
* More than 10 comments results in a load more posts button between comment bar and comments (loads ten comments then scroll and auto load as more scrolling/comment review is desired.