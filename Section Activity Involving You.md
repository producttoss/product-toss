# Activity Involving You Screen #

Activity Involving You is a user's page that displays events where the target = user.

## Mockups ##
![activity 1.jpg](https://bitbucket.org/repo/e96xBk/images/125822236-activity%201.jpg)
Delete single notification does not require a confirmation.

![Activity 2.jpg](https://bitbucket.org/repo/e96xBk/images/2715382078-Activity%202.jpg)

![activity 3.jpg](https://bitbucket.org/repo/e96xBk/images/1664277998-activity%203.jpg)
After accessing new notifications dash bar notifications icon looses orange fill in speech bubble^


## Entrances ##
* click Dash Bar button
* access from search screens through dash bar
* access from home screens through dash bar

## Exits ##
* click username
* click dashbar icons (all other than activity involving you icon)
* click profile icon in banner
* click user profile thumbnails (round-left side)
* click image thumbnails (square-right side)

## Actions ##
* respond to follow inquiries if private account
* delete individual notifications by swiping them left and clicking delete
* clear notifications (requires confirmation)
* follow (or request to follow if UserB account is private) users who followed you (post request confirmation if UserA private account)
* scroll through list of notifications

## Definitions ##
* list of events: all events that target the given user
* time of last access: the last time the user views this page is stored
* time of last event: the most recent events time is stored
* new events: if time of last event > time of last access, the user sees a notification
* no new events: if time of last access > time of last event, the user sees no notification
* page access: button in dash bar
* notification:

## Notes ##
* This page will be accessible from dash bar.
* Any time an activity involving you occurs the button in the dash bar will appear upon entry into the app or refresh of the app with a colored notification appearance.
* Notification will disappear after page is accessed.

### List of events ###
* User follow (you may follow them back directly from this page or access their page first by clicking their username)
* User request to follow (if private account) (you may first approve permission from this page and then second step follow them back from this page as well) (you may also access their profile first if you would like to review their profile before allowing them permissions **if user has private account and requests to follow you: their profile becomes accessible to your view.. If user cancels request you lose access to view their profile) If someone requests to follow you and you do not want to permit them access you may either block them or just take no action to their request.
* User mentions your username in a comment
* User comments on your photo
* User comments on your video
* User sends you a direct post (this is the only place a direct post will be accessible from)(direct posts will be treated same as any other post only will be viewable to a specific audience determined by user making post)
* Thumbnails on right side of screen link to associated posts
* Usernames link to user profiles

## API ##
* getUserEvents
    * Input: sessionId
    * Return: error state, list of userEvents
* clearUserEvents
    * Input: sessionId
    * Return: error state
    * Side effect: update user's userEventClearDate
* hideUserEvent
    * Input: sessionId, userEventId?
    * Return: error state
    * Side effect: update user's notificationHideList
* requestFollow
    * Input: sessionId, userId
* acceptFollow
    * Input: sessionId, userId
    

## DB ##
* session table
    * lookup userId by sessionId
* user meta-data table (registration?)
    * lookup userEvent clear date, list of hidden userEvents
    * update clear date 
    * add hidden notification to list
* follow table
    * add follow between UserA and UserB
    * update request to follow between UserA and UserB
    * add follow request between UserA and UserB
* userEvents table ?
    * lookup notifications by userId, userEventsClearDate
    * Issues:
        * How many records can a user have?
        * How can these be deleted from a table cleanly?
        * RR with some max #?  30 day ttl?
        * Or do you poll for new events (from various locations) and insert them into userEvents table @ request time?
        * Or insert them at generation time?
